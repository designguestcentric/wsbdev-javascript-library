/*
 
 Custom shorthand JS plugin
 ****************************
 JQuery 1.10.2
 Author: pedro.d
 Date: 16/02/2017
 Latest update: 31/03/2017 by margarida.p && tiago.o
 Latest update: 25/05/2017 by pedro.d && ana.l (re-do fuction to fadeOut calendard - line 523)
 Latest update: 14/07/2017 by pedro.d add selectCheckout element + add body class for date-selected
 Latest update: 17/11/2017 by pedro.d add formURL as a parameter for custom secured BE url's (e.g. Heritage Hotels)
 Latest update: 18/11/2017 by pedro.d prevent contact form button to validate hotel select apikey
 Latest update: 08/02/2018 by pedro.d plugin locatization language
 Latest update: 16/02/2018 by pedro.d hack for datepicker localization, because website render is adding the correct lang for pt-BR
 Latest update: 20/03/2018 by pedro.d add date placeholder withou check-out date available
 Latest update: 30/04/2018 by pedro.d fixed issue to nr adults default parameter

 */

(function($){

	var jQuery = $;

	// custom shorthand plugin
	$.fn.gcBookingGadgetCustom = function( options, data ) {

		// default options
		var settings = $.extend({
			// list of hotels
			hotels: [
				{ name: 'Add hotel name on template', apikey: '' } // default if none was added in the template js
			],
			// default form fields
			checkin: true,
			checkout: true,
			nights: false,
			amount: false,
			adults: true,
			children: true,
			promocode: false,
			// default option values
			formTarget: '_self',
			numberOfMonths: [1,2],
			dateFormat: 'dd-mm-yy',
			numberOfAdults: 2,
			numberOfNights: 2,
			// custom classes
			selectHotelClass: '',
			selectCheckinClass: '',
			selectCheckoutClass: '',
			selectNightsClass: '',
			selectAmountClass: '',
			selectAdultsClass: '',
			selectChildrenClass: '',
			selectPromocodeClass: '',
			submitButtonClass: '',
			// secure booking engine URL
			formUrl: 'https://secure.guestcentric.net/api/bg/book.php'
		}, options );


		if ($(window).width() <= 768) {
			// change number of months on mobile
			settings.numberOfMonths = 1;
		}

		// html strucuture
		var	formNode = $(document.createElement('form')),
			lang = $("meta[http-equiv=content-language]").attr("content");

			// hack for datepicker localization, because website render is adding the correct lang for pt-BR - pedro.d 13-02-2018
			if (lang == 'br') {
				lang = 'pt-BR';
			}

		// labels
		var gcBookingGadgetCustom_labels = {
			"en": {
				"hotel": "Hotel",
				"choose": "Choose",
				"checkin": "Check-in",
				"checkout": "Check-out",
				"nights": "Nr. Nights",
				"amount": "Amount",
				"adults": "Adults",
				"children": "Children",
				"promocode": "Promo Code",
				"booknow": "Book Online",
				"bar": "Best Available Rate",
				"selectCheckout": "Select your check-out date."
			},
			"fr": {
				"hotel": "Hôtel",
				"choose": "Choisir",
				"checkin": "Date d'arrivée",
				"checkout": "Date du départ",
				"nights": "Nuitée(s)",
				"amount": "Montant",
				"adults": "Adultes",
				"children": "Enfants",
				"promocode": "Code Promo",
				"booknow": "Réserver",
				"bar": "Meilleur Tarif Garanti",
				"selectCheckout": "Sélectionnez votre date de départ."
			},
			"de": {
				"hotel": "Hotel",
				"choose": "Wählen",
				"checkin": "Anreise",
				"checkout": "Abreise",
				"nights": "Nr. Nächte",
				"amount": "Betrag",
				"adults": "Erwachsene",
				"children": "Kinder",
				"promocode": "Aktionscode",
				"booknow": "Buchen",
				"bar": "Bestpreisgarantie",
				"selectCheckout": "Wählen Sie Ihr Abreisedatum aus."
			},
			"it": {
				"hotel": "Hotel",
				"choose": "Scegliere",
				"checkin": "Check-in",
				"checkout": "Check-out",
				"nights": "Nr. di notti",
				"amount": "Importo",
				"adults": "Adulti",
				"children": "Bambini",
				"promocode": "I codici promozionali",
				"booknow": "Prenota ora",
				"bar": "Migliore Tariffa Disponibile",
				"selectCheckout": "Seleziona la data di check-out."
			},
			"zh-CN": {
				"hotel": "酒店",
				"choose": "选择",
				"checkin": "入住手续",
				"checkout": "离店结帐手续",
				"nights": "多少晚",
				"amount": "总数",
				"adults": "每间客房的成年人数",
				"children": "每间客房的未成年人数",
				"promocode": "促销代码",
				"booknow": "现在预定",
				"bar": "最优惠价格保证",
				"selectCheckout": "选择您的退房日期."
			},
			"pl": {
				"hotel": "Hotel",
				"choose": "Wybierać",
				"checkin": "Zameldowanie",
				"checkout": "Wymeldowanie",
				"nights": "Liczba nocy",
				"amount": "Kwota",
				"adults": "Dorośli",
				"children": "Dzieci",
				"promocode": "Kody promocyjne",
				"booknow": "Zarezerwuj",
				"bar": "Gwarancja najlepszej ceny",
				"selectCheckout": "Wybierz datę wyjazdu."
			},
			"pt": {
				"hotel": "Hotel",
				"choose": "Escolha",
				"checkin": "Data entrada",
				"checkout": "Data saída",
				"nights": "Nr. de noites",
				"amount": "Quantidade",
				"adults": "Adultos",
				"children": "Crianças",
				"promocode": "Código promocional",
				"booknow": "Reserva Online",
				"bar": "Melhor Tarifa Disponível",
				"selectCheckout": "Selecione a data de saída."
			},
			"ru": {
				"hotel": "Гостиница",
				"choose": "выберите",
				"checkin": "Заезд",
				"checkout": "Выселение",
				"nights": "Количество ночей",
				"amount": "Количество",
				"adults": "Кол-во Взрослых",
				"children": "Детей на номер",
				"promocode": "Промокоды",
				"booknow": "Забронировать сейчас",
				"bar": "Самый низкий доступный тариф",
				"selectCheckout": "Выберите дату выезда."
			},
			"es": {
				"hotel": "Hotel",
				"choose": "Escoger",
				"checkin": "Fecha de llegada",
				"checkout": "Fecha de salida",
				"nights": "N. de noches",
				"amount": "Cantidad",
				"adults": "Adultos",
				"children": "Niños",
				"promocode": "Los códigos promocionales",
				"booknow": "Reserva Online",
				"bar": "La mejor tarifa garantizada",
				"selectCheckout": "Seleccione su fecha de salida."
			},
			"tr": {
				"hotel": "Otel",
				"choose": "Seçmek",
				"checkin": "Giriş",
				"checkout": "Çıkış",
				"nights": "Gece sayısı",
				"amount": "Oda sayısı",
				"adults": "Oda başına yetişkinler",
				"children": "Oda başına çocuklar",
				"promocode": "Promosyon kodları",
				"booknow": "Şimdi ayırt",
				"bar": "En iyi oran garantili",
				"selectCheckout": "SÇıkış tarihini seç."
			},
			"ca": {
				"hotel": "Hotel",
				"choose": "Triar",
				"checkin": "Data d'arribada",
				"checkout": "Data de sortida",
				"nights": "Núm. de nits",
				"amount": "Quantitat",
				"adults": "Adults",
				"children": "Nens",
				"promocode": "Els codis promocionals",
				"booknow": "Reservar",
				"bar": "Millor tarifa garantida",
				"selectCheckout": "Seleccioneu la vostra data de sortida."
			},
			"fi": {
				"hotel": "Hotelli",
				"choose": "Valita",
				"checkin": "Saapumispäivä",
				"checkout": "Lähtöpäivä",
				"nights": "Öiden määrä",
				"amount": "Huoneiden määrä",
				"adults": "Aikuisia",
				"children": "Lapsia",
				"promocode": "Tarjouskoodit",
				"booknow": "Varaa nyt",
				"bar": "Paras määrä taata",
				"selectCheckout": "Valitse päivämäärät."
			},
			"pt-BR": {
				"hotel": "Hotel",
				"choose": "Escolha",
				"checkin": "Data entrada",
				"checkout": "Data saída",
				"nights": "Nr. de noites",
				"amount": "Quantidade",
				"adults": "Adultos",
				"children": "Crianças",
				"promocode": "Código promocional",
				"booknow": "Reserva Online",
				"bar": "Melhor preço garantido",
				"selectCheckout": "Selecione sua data de saída."
			},
			"el": {
				"hotel": "ξενοδοχειο",
				"choose": "επιλέγω",
				"checkin": "Άφιξη",
				"checkout": "Αναχώρηση",
				"nights": "Νύχτες",
				"amount": "Ποσο",
				"adults": "Ενήλικες",
				"children": "Παιδιά",
				"promocode": "Διαφημιστικά κώδικες",
				"booknow": "Κάντε κράτηση",
				"bar": "Εγγυημένη χαμηλότερη τιμή",
				"selectCheckout": "Επιλέξτε την ημερομηνία αναχώρησης."
			},
			"lv": {
				"hotel": "Viesnīca",
				"choose": "Izvēlēties",
				"checkin": "Ierašanās",
				"checkout": "Izrakstīšanās",
				"nights": "Nakšu skaits",
				"amount": "Numuru skaits",
				"adults": "Pieaugušo",
				"children": "Bērnu",
				"promocode": "Īpašo piedāvājumu kodi",
				"booknow": "Rezervēt tagad",
				"bar": "Garantēta labākā cena",
				"selectCheckout": "Izvēlieties izrakstīšanās datumu."
			},
			"sv": {
				"hotel": "Hotell",
				"choose": "Välja",
				"checkin": "Checka in",
				"checkout": "Checka ut",
				"nights": "Nr. nätter",
				"amount": "Antal rum",
				"adults": "Vuxna",
				"children": "Barn",
				"promocode": "PR koder",
				"booknow": "Boka nu",
				"bar": "Bästa hastighet garanterad",
				"selectCheckout": "Vyberte dátum odhlásenia."
			}
		};

		// creating html templating - check html file template for understanding structure: gcBookingGadgetCustom.html
		// create check-in elements
		if (settings.checkin) {
			formNode.append($(document.createElement('div')).addClass('selectField selectDate selectCheckin')
				.append($(document.createElement('div')).addClass('datepicker'))
				.append($(document.createElement('label')).attr('for', 'selectCheckin').html([gcBookingGadgetCustom_labels[lang].checkin]))
				.append($(document.createElement('input')).attr('type','text').attr('id', 'checkin').attr('name', 'startDay').addClass('calinput'))
					.append($(document.createElement('span')).addClass('calicon calendarCheckin')
				)
			)
		}
		// create check-out elements
		if (settings.checkout) {
			formNode.append($(document.createElement('div')).addClass('selectField selectDate selectCheckout')
				.append($(document.createElement('label')).attr('for', 'selectCheckout').html([gcBookingGadgetCustom_labels[lang].checkout]))
				.append($(document.createElement('input')).attr('type','text').attr('id', 'checkout').addClass('calinput'))
					.append($(document.createElement('span')).addClass('calicon calendarCheckout')
				)
			)
		}
		// create number of nights elements
		if (settings.nights) {
			formNode.append($(document.createElement('div')).addClass('selectField selectNights')
				.append($(document.createElement('label')).attr('for', 'selectNights').html([gcBookingGadgetCustom_labels[lang].nights]))
				.append($(document.createElement('select')).attr('name', 'nrNights')
					.append($(document.createElement('option')).attr('value', '1').html('1'))
					.append($(document.createElement('option')).attr('value', '2').html('2').attr('selected', 'selected'))
					.append($(document.createElement('option')).attr('value', '3').html('3'))
					.append($(document.createElement('option')).attr('value', '4').html('4'))
					.append($(document.createElement('option')).attr('value', '5').html('5'))
					.append($(document.createElement('option')).attr('value', '6').html('6'))
					.append($(document.createElement('option')).attr('value', '7').html('7'))
					.append($(document.createElement('option')).attr('value', '8').html('8'))
					.append($(document.createElement('option')).attr('value', '9').html('9'))
					.append($(document.createElement('option')).attr('value', '10').html('10'))
					.append($(document.createElement('option')).attr('value', '11').html('11'))
					.append($(document.createElement('option')).attr('value', '12').html('12'))
					.append($(document.createElement('option')).attr('value', '13').html('13'))
					.append($(document.createElement('option')).attr('value', '14').html('14'))
					.append($(document.createElement('option')).attr('value', '15').html('15'))
				)
			)
		} else {
			formNode.append($(document.createElement('input')).attr('type','hidden').attr('name','nrNights').attr('value',settings.numberOfNights));
		}
		// create amount elements
		if (settings.amount) {
			formNode.append($(document.createElement('div')).addClass('selectField selectAmount')
				.append($(document.createElement('label')).attr('for', 'selectAmount').html([gcBookingGadgetCustom_labels[lang].amount]))
				.append($(document.createElement('select')).attr('name', 'amount')
					.append($(document.createElement('option')).attr('value', '1').html('1'))
					.append($(document.createElement('option')).attr('value', '2').html('2'))
					.append($(document.createElement('option')).attr('value', '3').html('3'))
					.append($(document.createElement('option')).attr('value', '4').html('4'))
				)
			)
		}
		// create number of adults elements
		if (settings.adults) {

			formNode.append($(document.createElement('div')).addClass('selectField selectAdults')
				.append($(document.createElement('label')).attr('for', 'selectAdults').html([gcBookingGadgetCustom_labels[lang].adults]))
				.append($(document.createElement('select')).attr('name', 'nrAdults')
					.append($(document.createElement('option')).attr('value', '1').html('1'))
					.append($(document.createElement('option')).attr('value', '2').html('2'))
					.append($(document.createElement('option')).attr('value', '3').html('3'))
					.append($(document.createElement('option')).attr('value', '4').html('4'))
					.append($(document.createElement('option')).attr('value', '5').html('5'))
					.append($(document.createElement('option')).attr('value', '6').html('6'))
				)
			)

			
			// select default number of adults
			var nrAdultsElem = setInterval(function() {
				if ($("select[name='nrAdults']").length > 0) {

					clearInterval(nrAdultsElem);

					$("select[name='nrAdults']").find("option").each( function() {
						if ($(this).val() == settings.numberOfAdults) {
							$(this).attr("selected", true);
						}
					});
				}
			}, 500);
			
		} else {
			formNode.append($(document.createElement('input')).attr('type','hidden').attr('name','nrAdults').attr('value',settings.numberOfAdults));
		}
		// create number of children elements
		if (settings.children) {
			formNode.append($(document.createElement('div')).addClass('selectField selectChildren')
				.append($(document.createElement('label')).attr('for', 'selectChildren').html([gcBookingGadgetCustom_labels[lang].children]))
				.append($(document.createElement('select')).attr('name', 'nrChildren')
					.append($(document.createElement('option')).attr('value', '0').html('0'))
					.append($(document.createElement('option')).attr('value', '1').html('1'))
					.append($(document.createElement('option')).attr('value', '2').html('2'))
					.append($(document.createElement('option')).attr('value', '3').html('3'))
					.append($(document.createElement('option')).attr('value', '4').html('4'))
				)
			)
		}
		// create promo codes elements
		if (settings.promocode) {
			formNode.append($(document.createElement('div')).addClass('selectField selectPromocode')
				.append($(document.createElement('label')).attr('for', 'selectPromocode').html([gcBookingGadgetCustom_labels[lang].promocode]))
				.append($(document.createElement('input')).attr('type','text').attr('id', 'pc').attr('name','pc'))
			)
		}

		// create hotel and book now button elements - not optional params
		formNode.prepend($(document.createElement('div')).addClass('selectField selectHotel')
				.append($(document.createElement('label')).attr('for', 'selectHotel').html([gcBookingGadgetCustom_labels[lang].hotel]))
				.append($(document.createElement('select')).attr('name', 'apikey')
					.append($(document.createElement('option')).attr('id', 'chooseHotel').attr('value', '').html([gcBookingGadgetCustom_labels[lang].choose]))
				)
			)
			.append($(document.createElement('div')).addClass('selectField submitButton')
			.append($(document.createElement('button')).attr('type','submit').attr('class', 'booknow')
				.append($(document.createElement('span')).html([gcBookingGadgetCustom_labels[lang].booknow])))
			.append($(document.createElement('label')).attr('for','bar').addClass('bar')))
			// create hidden language value
			.append($(document.createElement('input')).attr('type','hidden').attr('name', 'l'))
			// create hidden referral value
			.append($(document.createElement('input')).attr('type','hidden').attr('name', 'r'))
		.attr('action', settings.formUrl).attr('method','get').attr('target', settings.formTarget);


		// create html template
		$(document.createElement('div')).append(formNode).addClass('gcBookingGadgetCustom').prependTo(this);
				
		// elements after created
		var	datepickerElem = $(".datepicker"),
			checkin = $("#checkin"),
			checkout = $("#checkout"),
			hotelSelect = $("select[name='apikey']"),
			selectHotel = $(".selectHotel"),
			chooseHotel = $("#chooseHotel"),
			selectCheckin = $(".selectCheckin"),
			selectCheckout = $(".selectCheckout"),
			selectNights = $(".selectNights"),
			selectAmount = $(".selectAmount"),
			selectAdults = $(".selectAdults"),
			selectChildren = $(".selectChildren"),
			selectPromocode = $(".selectPromocode"),
			submitButton = $(".submitButton"),
			barLabel = $(".bar"),
			nrNights = $("select[name='nrNights']");


		// add hotels to select
		var thisHotels = settings.hotels;
		for(var i = 0; i < thisHotels.length; i++) {
			hotelSelect.append($('<option>', {value:thisHotels[i].apikey, text:thisHotels[i].name}));	
		}

		// default nr of nights
		nrNights.val(settings.numberOfNights);

		// insert labels
		selectHotel.find("label").text(gcBookingGadgetCustom_labels[lang].hotel);
		chooseHotel.text(gcBookingGadgetCustom_labels[lang].choose);
		selectCheckin.find("label").text(gcBookingGadgetCustom_labels[lang].checkin);
		selectCheckout.find("label").text(gcBookingGadgetCustom_labels[lang].checkout);
		selectNights.find("label").text(gcBookingGadgetCustom_labels[lang].nights);
		selectAmount.find("label").text(gcBookingGadgetCustom_labels[lang].amount);
		selectAdults.find("label").text(gcBookingGadgetCustom_labels[lang].adults);
		selectChildren.find("label").text(gcBookingGadgetCustom_labels[lang].children);
		selectPromocode.find("label").text(gcBookingGadgetCustom_labels[lang].promocode);
		submitButton.find("span").text(gcBookingGadgetCustom_labels[lang].booknow);
		barLabel.text(gcBookingGadgetCustom_labels[lang].bar);

		// add template classes	
		selectHotel.addClass(settings.selectHotelClass);
		selectCheckin.addClass(settings.selectCheckinClass);
		selectCheckout.addClass(settings.selectCheckoutClass);
		selectNights.addClass(settings.selectNightsClass);
		selectAmount.addClass(settings.selectAmountClass);
		selectAdults.addClass(settings.selectAdultsClass);
		selectChildren.addClass(settings.selectChildrenClass);
		selectPromocode.addClass(settings.selectPromocodeClass);
		submitButton.addClass(settings.submitButtonClass);

		// load datepicker files
		$.getScript( "//code.jquery.com/ui/1.12.1/jquery-ui.min.js" ).done(function( script, textStatus ) {

			// load script for plugin locatization
			$.getScript( "//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.8.13/i18n/jquery-ui-i18n.min.js" ).done(function( script, textStatus ) {
				
				// import datepicker default css file
				$('head').append('<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css" type="text/css" />');


				// init calendar
				var options = $.extend(
					{},
					$.datepicker.regional[lang],
					{ dateFormat: settings.dateFormat }
				);
				$.datepicker.setDefaults(options);







				if (settings.checkout) {

					datepickerElem.datepicker({
						minDate: 0,
						numberOfMonths: settings.numberOfMonths,
						beforeShowDay: function(date) {
							var date1 = $.datepicker.parseDate($.datepicker._defaults.dateFormat, checkin.val());
							var date2 = $.datepicker.parseDate($.datepicker._defaults.dateFormat, checkout.val());

							checkin.attr("placeholder", $.datepicker.formatDate(settings.dateFormat, new Date()));
							var checkoutdate = new Date();
							
							checkoutdate.setDate(checkoutdate.getDate() + 2);
							checkout.attr("placeholder", $.datepicker.formatDate(settings.dateFormat, checkoutdate));
							return [true, date1 && ((date.getTime() == date1.getTime()) || (date2 && date >= date1 && date <= date2)) ? "dp-highlight" : ""];
						},
						onSelect: function(dateText, inst) {
							var date1 = $.datepicker.parseDate($.datepicker._defaults.dateFormat, checkin.val());
							var date2 = $.datepicker.parseDate($.datepicker._defaults.dateFormat, checkout.val());
							var selectedDate = $.datepicker.parseDate($.datepicker._defaults.dateFormat, dateText);
							

							if (!date1 || date2) {
								checkin.val(dateText);
								checkout.val("");
								datepickerElem.addClass("selectCheckout");
								datepickerElem.append("<div class='legend_selcheckout'>"+gcBookingGadgetCustom_labels[lang].selectCheckout+"</div>");
								$(this).datepicker();
							} else if( selectedDate < date1 ) {
								checkout.val( checkin.val() );
								checkin.val( dateText );
								$(this).datepicker();
								datepickerElem.fadeOut();
								datepickerElem.removeClass("selectCheckout");
								$(".legend_selcheckout").remove();
								$("body").addClass("date-selected");
							} else {
								checkout.val(dateText);
								$(this).datepicker();
								datepickerElem.fadeOut();
								datepickerElem.removeClass("selectCheckout");
								$(".legend_selcheckout").remove();
								$("body").addClass("date-selected");
							}
							showDays();
						}
					});

					// click to open datepicker
					$(".calinput").click( function() {
						datepickerElem.fadeIn();
					});

					// calc number of nights
					function showDays() {
						var date1 = $.datepicker.parseDate($.datepicker._defaults.dateFormat, checkin.val());
						var date2 = $.datepicker.parseDate($.datepicker._defaults.dateFormat, checkout.val());
						if (!date1 || !date2) return;
						var days = (date2 - date1) / 1000 / 60 / 60 / 24;
						// nrNights.val(days);
						if (settings.nights) {
							nrNights.find("option[value='"+days+"']").val(days).prop('selected', true);
						} else {
							$('input[name="nrNights"]').val(days);
						}
					}

				} else {
					datepickerElem.datepicker({
						minDate: 0,
						numberOfMonths: settings.numberOfMonths,
						onSelect: function(dateText, inst) {
							var selectedDate = $.datepicker.parseDate($.datepicker._defaults.dateFormat, dateText);
							checkin.val( dateText );
							datepickerElem.fadeOut();
						}
					});

					$("#checkin").click(function() {
						datepickerElem.fadeIn();
					});

					// TODO - show today's date on check in calendar only
					datepickerElem.datepicker.beforeShowDay = function(date) {
						checkin.attr("placeholder", $.datepicker.formatDate(settings.dateFormat, new Date()));
					}
					datepickerElem.datepicker.beforeShowDay()
					
				}

				$(document).click(function(e) { 
					// var elemClicked = $(e.toElement);
					var elemClicked = $(e.target);

					// console.log(elemClicked);
					// console.log(!elemClicked.is("#checkin"))

					if (!elemClicked.is("#checkin") && !elemClicked.is("#checkout") && !elemClicked.hasClass("hasDatepicker") && !elemClicked.hasClass("ui-datepicker") && !elemClicked.hasClass("ui-icon") && !elemClicked.hasClass("ui-datepicker-prev") && !elemClicked.hasClass("ui-datepicker-next") && !$(elemClicked).parent().parents(".ui-datepicker").length) {
						$(".datepicker").fadeOut();
					}
				});

				// if hotel not selected
				hotelSelect.change( function() {
					if (!hotelSelect.val()) {
						hotelSelect.addClass("error").css('color', 'red');
					} else {
						hotelSelect.removeClass("error").css('color', 'inherit');
					}
				});

				// form validation onclick
				submitButton.find("button.booknow").click( function() {

					$('.gcBookingGadgetCustom form').submit(function () {

						// get URL for referral param
						var protocol = window.location.protocol,
							url = window.location.hostname;

						if (!hotelSelect.val()) {
							hotelSelect.addClass("error").css('color', 'red');
							return false;
						} else {
							$("input[name='l']").val(lang);
							$("input[name='r']").val(''+protocol+'//'+url);

							// console.log($('input[name="startDay"]').val());
							// console.log($('select[name="apikey"]').val());
							// console.log($('select[name="nrNights"]').val());

							// console.log($("input[name='l']").val());
							// console.log($("input[name='r']").val());
							
							return true;
						}
					});

				});

			}).fail(function( jqxhr, settings, exception ) {
				console.log("missing jqueryui plugin file");
			});

		}).fail(function( jqxhr, settings, exception ) {
			console.log("missing jqueryui plugin file");
		});
	};

})(jQuery);