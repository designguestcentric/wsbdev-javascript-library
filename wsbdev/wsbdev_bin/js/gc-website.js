/*
 
 Websites Javascript Library
 ****************************
 JQuery 1.10.2
 Author: tiago.o and pedro.d
 Date: 20/05/2015
 Latest update: 16/03/2018 by pedro.d
 Comment: adding hideOffersWebsite() function for * hide offers

 */


var gc = window.gc || {};

(function () {

    gc.website = (function () {

        var data = {
                geo: null,
                sliderLoaded: false,
                pages: {
                    current: 'Home',
                    matchObj: {
                        'homepage': 'Homepage',
                        'hotel-rooms': 'Rooms',
                        'hotel-special-offers': 'Offers',
                        'hotel-products': 'Products',
                        'hotel-addons': 'Addons',
                        'hotel-gallery': 'Gallery',
                        'hotel-directions': 'Location',
                        'hotel-contacts': 'Contacts'
                    }
                }
            },

            interface = {
                browserWidth: null,
                    browserHeight: null,
                    pageWidth: null,
                    pageHeight: null
            };

        function setHtmlVars() {

            var bodyNode = $(document.body);
            var htmlObj = {
                bodyNode: bodyNode,
                topBlock: $('#topblock'),
                centerBlock: $('#centerblock'),
                contentBlock: $('#contentblock'),
                bottomBlock: $('#bottomblock'),
                bannerNode: $(".flashContainer"),

                navigationNode: $(".navigation"),
                quicklinksNode: $(".quicklinks"),

                imageLink: $(".imagelinkgadget"),
                imageLink_Large: $(".large"),
                imageLink_MediumLarge: $(".mediumlarge"),
                imageLink_Medium: $(".medium"),
                imageLink_Small: $(".small"),
                imageLink_Popup: $(".popup"),

                map: $("#GoogleMapGadget"),

                hasBanner: bodyNode.hasClass("with-bannerGadget"),
                hasBannerPromo: bodyNode.hasClass("with-bannerPromo"),
                hasBannerImg: bodyNode.hasClass("with-bannerImg"),
                hasBannerVideo: bodyNode.hasClass("with-bannerVideo"),
                hasNoBanner: bodyNode.hasClass("no-banner"),

                url: window.location.hostname,

                clickHandler: "click"

            };

            $.extend(interface, htmlObj);
        };

        function setCurrentPage() {
            for (var page in data.pages.matchObj) {
                if (interface.bodyNode.hasClass(page)) {
                    data.pages.current = data.pages.matchObj[page];
                    break;
                }
            }
        };

        function resize() {
            interface.browserWidth = $(window).width();
            interface.browserHeight = $(window).height();
            interface.pageWidth = $(document.body).width();
            interface.pageHeight = $(document.body).height();
        };

        function mobileClass() {

            if (interface.browserWidth <= 992) {
                $(interface.bodyNode).addClass("mobile");
            } else {
                $(interface.bodyNode).removeClass("mobile");
            }
        };

        function bannerSize(obj) {
            if (interface.hasBanner || interface.hasBannerPromo || interface.hasBannerImg || interface.hasBannerVideo || data.pages.current == 'Location') {

                interface.centerBlock.css('height', interface.browserHeight - obj.varBannerHeight1 - obj.varBannerHeight2 + "px");

                if (interface.browserWidth > 992) {
                    if (interface.centerBlock.css('position') == 'absolute' || interface.centerBlock.css('position') == 'fixed') {
                        // you have to fix max-height and margin-top issues on template
                        // go there, I'm sure it won't bite!!!! and you'll be an better web designer
                    } else {
                        var originalWidth = 1280,
                            originalHeight = 720;

                        // calucale 16x9 proportion
                        maxHeight = (originalHeight / originalWidth) * interface.browserWidth;

                        interface.centerBlock.css('max-height', maxHeight + "px");
                    }
                }


                interface.contentBlock.css("visibility", "visible");
            } else {
                interface.centerBlock.css("height", 0 + "px");
            }
        };

        function windowScroll(obj) {
            //window scroll
            var scroll = $(window).scrollTop(),
                isLocation = interface.bodyNode.hasClass("hotel-directions");

            if (!isLocation) {

                if (interface.hasBanner || interface.hasBannerPromo || interface.hasBannerImg || interface.hasBannerVideo) {
                    if (scroll > 0) {
                        interface.bodyNode.addClass("scrolling");
                    } else {
                        interface.bodyNode.removeClass("scrolling");
                    }
                    if (scroll > obj.varScroll1) {
                        interface.bodyNode.addClass("scroll1");
                    } else {
                        interface.bodyNode.removeClass("scroll1");
                    }
                    if (scroll > obj.varScroll2) {
                        interface.bodyNode.addClass("scroll2");
                    } else {
                        interface.bodyNode.removeClass("scroll2");
                    }
                    if (scroll > interface.browserHeight) {
                        interface.bodyNode.addClass("stage1");
                    } else {
                        interface.bodyNode.removeClass("stage1");
                    }
                }
            } else {
                if (interface.hasBanner || interface.hasBannerPromo || interface.hasBannerImg) {

                    if (scroll > interface.browserHeight) {
                        interface.bodyNode.addClass("stage1");
                    } else {
                        interface.bodyNode.removeClass("stage1");
                    }
                }
            }

        };



        function isTouchDevice() {
            return 'ontouchstart' in document.documentElement;
        }

        function handlerEvents(obj) {

            var _object = obj;

            //event resize
            $(window).resize(function () {
                resize();
                bannerSize(_object);
                mobileClass();
            });

            //event scroll
            $(window).scroll(function () {
                windowScroll(_object);
            });

            //button scroll
            $('#scroll').click(function () {
                if (!interface.bodyNode.hasClass('scrolling')) {
                    $('html,body').animate({
                        scrollTop: interface.browserHeight - _object.scrollFixedOffset
                    }, 600);
                } else {
                    $('html,body').animate({
                        scrollTop: 0
                    }, 600);
                }
            });

            // add class hover on link
            interface.imageLink.hover(function () {
                if (!$(this).hasClass("nolink") == true) {
                    $(this).addClass("hover");
                }
            }, function () {
                $(this).removeClass("hover");
            });

            // detect if touch devive
            if (isTouchDevice()) {
                // on Mobile
                interface.clickHandler = "touchstart";
                interface.bodyNode.addClass("isTouchDevice");
            }

            // remove security seals from all websites
            $(".security").remove();

        };


        // function for adding UTM parameters to booking engine link - by marcelo.a 6-3-2018
        function setLinkerParam() {
            var linkerParam = '';

            if (bginit != undefined && bginit.vars.linkerParam == undefined) {
                var ga = window[window['GoogleAnalyticsObject']];
                bginit.vars.linkerParam = '';

                if (ga && ga.getAll) {
                    bginit.vars.linkerParam = ga.getAll()[0].get('linkerParam');
                    linkerParam = '&' + bginit.vars.linkerParam;
                }
            }

            return linkerParam;
        }


        function handlerDetails(obj) {

            var _object = obj;

            // banner size
            bannerSize(obj);

            // add class if mobile
            mobileClass(obj);

            // bigger photo
            $(".pgImage").each(function () {
                $(this).css('background-image', 'url("' + $(this).find("a").attr('href') + '")');
            });

            $(".pgText").click(function () {
                $(this).siblings(".pgImage").find("> a:first-child")[0].click();
            });

            $(".roomPhoto > a").each(function () {
                $(this).css('background-image', 'url("' + $(this).attr('href') + '")');
            });

            // imageLink popup
            if (interface.imageLink_Popup.length > 0) {
                var overlay = $("<div class='overlay'></div>");

                interface.bodyNode.addClass("popupWindow");
                interface.bodyNode.append(interface.imageLink_Popup);
                interface.bodyNode.append(overlay);
                interface.bodyNode.click(function () {
                    interface.bodyNode.removeClass("popupWindow");
                    interface.imageLink_Popup.remove();
                    overlay.remove();
                });
            }

            // add id attr to elems
            interface.imageLink_Small.each(function (i) {
                $(this).addClass('small' + i);
            });
            interface.imageLink_Medium.each(function (i) {
                $(this).addClass('medium' + i);
            });
            interface.imageLink_MediumLarge.each(function (i) {
                $(this).addClass('mediumlarge' + i);
            });
            interface.imageLink_Large.each(function (i) {
                $(this).addClass('large' + i);
            });

            //

        };

        function loading() {

            if (interface.hasNoBanner || interface.hasBannerImg) {
                $("#loader").fadeOut("slow");
                return;
            }

            var checker;
            checker = setInterval(function () {
                if (!interface.hasBannerImg && $(".gcb_container_sized").length > 0) {
                    var displayLoad = $('.gcb_image_loading').css('display');
                    if (displayLoad == 'none') {
                        $("#loader").fadeOut("slow");
                        interface.bodyNode.addClass("loaded");
                        clearInterval(checker);
                    }
                }
            }, 250);
        };

        function wsbTracker() {
            // all the events are now sending to gcTracker analytics account

            var scrollPos = $(window).scrollTop(),
                scrollArea = 'top',
                scrollStarted = false,
                // website pages
                page = 'Custom page',
                // shorthand
                shorthand = 'None',
                datepicker = 'None',
                calendInput = $("input[name=startDay], #shbgshortHandCalendarIcon"),
                nrnights = "Default",
                nrnightsInput = $("#shbgnrNights-button");

            // website pages
            if (data.pages.current == 'Homepage') {
                page = 'Homepage';
            } else if (data.pages.current == 'Rooms') {
                page = 'Rooms';
            } else if (data.pages.current == 'Offers') {
                page = 'Offers';
            } else if (data.pages.current == 'Products') {
                page = 'Products';
            } else if (data.pages.current == 'Addons') {
                page = 'Addons';
            } else if (data.pages.current == 'Gallery') {
                page = 'Gallery';
            } else if (data.pages.current == 'Location') {
                page = 'Location';
            } else if (data.pages.current == 'Contacts') {
                page = 'Contacts';
            }

            // shorthand
            if (calendInput.length > 0) {
                shorthand = 'Yes';
                // if calendar click
                calendInput.on('click', function () {
                    datepicker = 'Yes';
                });

                // if nr nights selection
                nrnightsInput.on('click', function () {
                    $(".yuimenuitemlabel").on('click', function () {
                        nrnights = $(this).text();
                        // console.log(nrnights);
                    });
                });
            }

            $(window).scroll(function () {
                scrollPos = $(window).scrollTop();

                if (!scrollStarted) {
                    scrollStarted = true;
                    // scroll started?
                    ga('gcTracker.send', 'event', page, 'Scrolled', scrollStarted, 1);
                }

                clearTimeout($.data(this, 'scrollTimer'));
                $.data(this, 'scrollTimer', setTimeout(function () {
                    if (scrollPos > interface.browserHeight && scrollPos < (interface.pageHeight - (interface.browserHeight * 2))) {
                        scrollArea = 'middle';
                    } else if (scrollPos > (interface.pageHeight - (interface.browserHeight * 2))) {
                        scrollArea = 'bottom';
                    }
                }, 500));

            });

            // menu clicked? Where? Is menu opened?
            $(".navigation a").bind("click", function (ev) {
                // check if menu is hidden
                var menuHidden = 'not hidden';
                if ($(".toggle").length > 0) {
                    menuHidden = 'hidden';
                }

                ga('gcTracker.send', 'event', page, 'Menu ' + menuHidden + ': clicked', scrollArea, 1);

            });

            // quicklinks clicked? Where? Is menu opened?
            $(".quicklinks a").bind("click", function (ev) {
                // check if menu is hidden
                var menuHidden = 'not hidden';
                if ($(".toggle").length > 0) {
                    menuHidden = 'hidden';
                }

                ga('gcTracker.send', 'event', page, 'Quicklinks: clicked (Menu ' + menuHidden + ')', scrollArea, 1);
            });

            // main book now button clicked
            $(".gc_bebutton, .shortHandCheckRates, #checkAvailability, .booknow").bind("click", function (ev) {

                // click during scroll, where?
                ga('gcTracker.send', 'event', page, 'Book now: clicked' + shorthand + '', scrollArea, 1);

                // if shorthand, clicked on datepicker and/or how many nights were selected?
                if (calendInput.length > 0) {
                    ga('gcTracker.send', 'event', page, 'Shorthand: ' + shorthand, 'Datepicker: ' + datepicker + ' Nights: ' + nrnights, 1);
                } else {
                    ga('gcTracker.send', 'event', page, 'Shorthand: ' + shorthand, '1', 1);
                }
            });

            // promotion widget clicked
            $(".promotionsgadget").bind("click", function (ev) {
                ga('gcTracker.send', 'event', page, 'Promotions box: clicked', scrollArea, 1);
            });

            // languages clicked
            $(".languages").bind("click", function (ev) {
                ga('gcTracker.send', 'event', page, 'Language: clicked', scrollArea, 1);
            });

            // scroll down/up clicked
            $("#scroll").bind("click", function (ev) {
                ga('gcTracker.send', 'event', page, 'Scroll: clicked', scrollArea, 1);
            });

            // opened the room?
            // rooms, offers, vouchers and add-ons
            $(".roomContainer").bind("click", function (ev) {
                ga('gcTracker.send', 'event', page, 'Room: opened', '1', 1);
            });
            // and clicked on the book now button?
            // rooms, offers, vouchers
            $(".promotions-bgLink").bind("click", function (ev) {
                ga('gcTracker.send', 'event', page, 'Room book now: clicked', '1', 1);
            });

        };

        function socialmediaLinks() {
            // website links to create social media
            var socialmedia = ["facebook", "twitter", "instagram", "youtube", "google", "pinterest", "linkedin", "foursquare", "flickr", "vimeo", "tripadvisor", "blog"],
                sml_target = $(".socialmedia");

            if (sml_target.find("> ul").length > 0) {
                sml_target = sml_target.find("> ul");
            }

            for (var index in socialmedia) {
                $('.footermenu li a[href*="' + socialmedia[index] + '"]').parent().addClass("" + socialmedia[index] + "").appendTo(sml_target);
            }

        };

        function mobilelandscapeAlert() {

            if (interface.bodyNode.hasClass('mobile')) {

                var bgColor = interface.bodyNode.css("background-color"),
                    txtColor = interface.bodyNode.css("color");

                if (interface.bodyNode.hasClass("lang-pt") || interface.bodyNode.hasClass("lang-br")) {
                    rotateMessage = "Rode o seu dispositivo para uma experiência mais otimizada.";
                } else if (interface.bodyNode.hasClass("lang-es")) {
                    rotateMessage = "Gire el dispositivo para obtener una experiencia de usuario optimizada.";
                } else if (interface.bodyNode.hasClass("lang-ca")) {
                    rotateMessage = "Si us plau, girar el dispositiu per a una experiència d'usuari optimitzada.";
                } else if (interface.bodyNode.hasClass("lang-fr")) {
                    rotateMessage = "Faites pivoter votre appareil pour une expérience utilisateur optimale.";
                } else if (interface.bodyNode.hasClass("lang-it")) {
                    rotateMessage = "Si prega di ruotare il dispositivo per un'esperienza utente ottimizzata.";
                } else if (interface.bodyNode.hasClass("lang-de")) {
                    rotateMessage = "Bitte drehen Sie Ihr Gerät für eine optimierte Benutzererfahrung.";
                } else if (interface.bodyNode.hasClass("lang-el")) {
                    rotateMessage = "Παρακαλούμε να περιστρέψετε τη συσκευή σας για καλύτερη εμπειρία του χρήστη.";
                } else if (interface.bodyNode.hasClass("lang-ru")) {
                    rotateMessage = "Поверните устройство, чтобы оптимизировать работу пользователя.";
                } else if (interface.bodyNode.hasClass("lang-zh-CN")) {
                    rotateMessage = "请旋转设备以获得优化的用户体验。";
                    // } else if (interface.bodyNode.hasClass("lang-pl")) {
                    // 	rotateMessage = "Please rotate your device for an optimized user experience.";
                    // } else if (interface.bodyNode.hasClass("lang-tr")) {
                    // 	rotateMessage = "Please rotate your device for an optimized user experience.";
                    // } else if (interface.bodyNode.hasClass("lang-fi")) {
                    // 	rotateMessage = "Please rotate your device for an optimized user experience.";
                    // } else if (interface.bodyNode.hasClass("lang-lv")) {
                    // 	rotateMessage = "Please rotate your device for an optimized user experience.";
                    // } else if (interface.bodyNode.hasClass("lang-sv")) {
                    // 	rotateMessage = "Please rotate your device for an optimized user experience.";
                    // } else if (interface.bodyNode.hasClass("lang-ro")) {
                    // 	rotateMessage = "Please rotate your device for an optimized user experience.";
                    // } else if (interface.bodyNode.hasClass("lang-sl")) {
                    // 	rotateMessage = "Please rotate your device for an optimized user experience.";
                } else {
                    rotateMessage = "Please rotate your device for an optimized user experience.";
                }

                $('<style>@media only screen and (max-width: 1480px) and (min-aspect-ratio: 13/9) { .show-on-landscape { display: table !important; } } .message-close{position: absolute;width: 30px;height: 30px;background: ' + txtColor + '; border-radius: 50%; top: 20px; right: 30px; text-indent: 9999px;} .message-close:before,.message-close:after {content: ""; width: 18px; border-bottom: 2px solid ' + bgColor + '; display: block; position: absolute; transform-origin: center center; top: 14px; left: 6px;} .message-close:before {transform: rotate(-45deg);}.message-close:after {transform: rotate(45deg);}</style>').appendTo('head');

                $('<div/>', {
                    'id': 'show-on-landscape',
                    'class': 'show-on-landscape',
                    'html': '<h2 style="display:table-cell !important; vertical-align: middle">' + rotateMessage + '</h2><a id="message-close" class="message-close">Close</a>',
                    'style': 'display: none; padding: 0 10%; box-sizing: border-box; position: fixed; top: 0; left: 0; width: 100%; height: 100%; background:' + bgColor + '; color:' + txtColor + '; z-index: 100002'
                }).appendTo(interface.bodyNode);

                $(".message-close").on("click", function () {
                    $(this).parent().remove();
                });
            }
        };

        function hideOffersWebsite() {
            if (data.pages.current == 'Rooms' || data.pages.current == 'Offers') {
                $(".roomName").find("h2:contains('*')").closest(".roomContainer").addClass('hide')
            }
        };

        // public functions
        return {

            data: null,

            init: function (obj, callback) {

                this.data = obj;

                setHtmlVars();
                resize();
                setCurrentPage();
                handlerDetails(obj);
                handlerEvents(obj);
                loading();
                wsbTracker();
                socialmediaLinks();
                mobilelandscapeAlert();
                hideOffersWebsite();

                if (callback)
                    callback();

            },

            initGallery: function (galleryNode) {
                return;
            },

            getHtmlNodes: function () {
                return interface;
            },

            getData: function () {
                return data;
            },

            getLocation: function (callback) {
                if (navigator.geolocation) {
                    navigator.geolocation.getCurrentPosition(function (position) {
                        data.geo = position;
                        if (callback) {
                            callback();
                        }
                    });
                } else {
                    alert("GeoLocation is not supported by this browser.");
                }
            },

            roomSlide: function (obj) {

                $.getScript("//static.guestcentric.net/cdn/wsbdev/wsbdev_bin/js/plugins/rooms/slick.min.js").done(function (script, textStatus) {

                    gc.website.initGallery = function (galleryNode) {
                        galleryNode.slick({
                            lazyLoad: 'ondemand'
                        });
                    }

                }).fail(function (jqxhr, settings, exception) {
                    console.log("missing roomSlide plugin file");
                });

            },

            galleryUnite: function (lib, obj) {

                $.getScript("//static.guestcentric.net/cdn/wsbdev/wsbdev_bin/js/plugins/gallery/unitegallery.min.js").done(function (script, textStatus) {
                    // import general css file
                    $('head').append('<link rel="stylesheet" href="//static.guestcentric.net/cdn/wsbdev/wsbdev_bin/js/plugins/gallery/unite-gallery.css" type="text/css" />');

                    $.getScript("//static.guestcentric.net/cdn/wsbdev/wsbdev_bin/js/plugins/gallery/" + lib + "/ug-theme-" + lib + ".js").done(function (script, textStatus) {
                        // redo html structure
                        $(".pgImage > a").each(function () {
                            var imageURL = $(this).attr("href"),
                                imageTitle = $(this).attr("title");

                            $("#pg").append('<img alt="' + imageTitle + '" src="' + imageURL + '" data-image="' + imageURL + '" data-description="' + imageTitle + '" />');
                        });
                        // remove old gallery elem
                        $(".pgContainer").remove();
                        // run plugin
                        $("#pg").unitegallery(obj);

                    }).fail(function (jqxhr, settings, exception) {
                        console.log("missing galleryUnite plugin file");
                    });

                }).fail(function (jqxhr, settings, exception) {
                    console.log("missing galleryUnite plugin file");
                });

            },

            galleryUniteAlbums: function (lib, obj) {

                $.getScript("//static.guestcentric.net/cdn/wsbdev/wsbdev_bin/js/plugins/gallery/unitegallery.min.js").done(function (script, textStatus) {
                    // import general css file
                    $('head').append('<link rel="stylesheet" href="//static.guestcentric.net/cdn/wsbdev/wsbdev_bin/js/plugins/gallery/unite-gallery.css" type="text/css" />');

                    $.getScript("//static.guestcentric.net/cdn/wsbdev/wsbdev_bin/js/plugins/gallery/" + lib + "/ug-theme-" + lib + ".js").done(function (script, textStatus) {
                        // redo html structure
                        $(".pgImage > a").each(function () {
                            var imageURL = $(this).attr("href"),
                                imageTitle = $(this).attr("title");

                            $(this).closest(".pgContainer").append('<img alt="' + imageTitle + '" src="' + imageURL + '" data-image="' + imageURL + '" data-description="' + imageTitle + '" />');
                        });

                        $(".pgContainer").each(function (i) {
                            // add id to main elem
                            $(this).attr('id', 'pgContainer' + i).fadeIn();
                            // create album title
                            albumTitle = $(this).find(".pgText").html();
                            $('<h3 class="galleryName">' + albumTitle + '</h3>').insertBefore($(this).closest(".pgContainer"));
                        });
                        // remove old gallery elem
                        $(".pgImage, .pgText").remove();
                        // run plugin
                        if (typeof UniteGalleryMain == 'function') {
                            $('div[id^="pgContainer"]').each(function () {
                                $(this).unitegallery(obj);
                            });
                        }

                    }).fail(function (jqxhr, settings, exception) {
                        console.log("missing galleryUnite plugin file");
                    });

                }).fail(function (jqxhr, settings, exception) {
                    console.log("missing galleryUnite plugin file");
                });

            },

            imglinkImageBg: function () {
                $(".imgImageWrapper").each(function () {
                    $(this).css('background-image', 'url("' + $(this).find("img").attr('src') + '")');
                    $(this).find("img").css("display", "none");
                });
            },

            movingGoogleMaps: function () {
                switch (data.pages.current) {
                    case 'Location':
                        interface.bodyNode.removeClass('with-bannerGadget with-bannerPromo with-bannerImg');
                        interface.map.find("#map_canvas").css('height', '100%');
                        interface.map.appendTo(interface.bannerNode).css({
                            'pointer-events': 'none',
                            'height': '100%'
                        });
                        interface.bannerNode.click(function () {
                            interface.map.css('pointer-events', 'auto');
                        });
                        break;
                    default:
                        break;
                }
            },

            pageSignature: function () {
                // adding signature name to text
                // get all url paramenters
                $.extend({
                    getUrlVars: function () {
                        var vars = [],
                            hash;
                        var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');

                        for (var i = 0; i < hashes.length; i++) {
                            hash = hashes[i].split('=');
                            vars.push(hash[0]);
                            vars[hash[0]] = hash[1];
                        }
                        return vars;
                    },
                    getUrlVar: function (name) {
                        return $.getUrlVars()[name];
                    }
                });
                // declare variabale
                var _firstName = $.getUrlVar("FirstName");
                // check if it exists
                if (_firstName != null) {
                    $("<p class='name_signature'>" + _firstName + "</p>").appendTo($(".content").first());
                }
            },

            mod01roomContainer: function () {
                // room toggle - revision
                $(".roomContainer").click(function () {
                    var _this = $(this);

                    if (!_this.hasClass("open")) {
                        var _clone = _this.clone(true),
                            bookingbt = _clone.find(".bgLink");

                        // replicate booking button
                        bookingbt.click(function () {
                            var rel = $(this).attr('rel'),
                                paramString = rel.match(/^bglink\[(.*?)\]/i)[1],
                                params = paramString.split(",");

                            window.location.href = bginit.vars.purl + '?apikey=' + bginit.vars.apikey + '&startDay=' + params[0] + '&nrNights=' + params[1] + '&preselect=' + params[2] + setLinkerParam();
                        });




                        // close all rooms and remove inactive elem
                        $(".roomContainer.room-opened").remove();
                        $(".roomContainer").removeClass("clicked open");
                        // clone, open and position roomContainer elem
                        _clone.prependTo(_this.parent()).addClass("room-opened clone open");
                        // add class to body
                        _this.addClass("clicked open");

                        gc.website.initGallery(_clone.find(".roomPhoto"));

                        // scroll to top
                        $('html,body').animate({
                            scrollTop: _clone.offset().top - gc.website.data.roomContainerOffsetTop
                        }, 600);
                    }

                });
            },

            mod02roomContainer: function () {

                // disabling the default room function
                $('.roomContainer').off("click");
                // re-arrange elements
                $(".roomContainer").each(function () {
                    var roomName = $(this).find(".roomName");
                    $(this).find(".minRate").insertBefore(roomName);
                    $("<a class='roomDetails'>Room Details</a>").insertAfter(roomName);
                });


                // new click function
                $(".roomPhoto, .roomName, .minRate, .roomDetails").on("click", function () {
                    var _this = $(this).closest(".roomContainer");
                    if (!_this.hasClass("room-opened")) {
                        // scroll to top
                        $('html,body').animate({
                            scrollTop: _this.offset().top - gc.website.data.roomContainerOffsetTop
                        }, 600);
                        // close all rooms and remove
                        // $(".roomContainer").removeClass("clicked room-opened");
                        // add class to roomContainer
                        _this.addClass("clicked room-opened");
                        // calling room gallery
                        gc.website.initGallery($(".roomContainer").find(".roomPhoto"));

                    }
                });
            },

            mod03roomContainer: function () {

                // sub-nav click to scroll
                $("#subnavigation").find("a").on("click", function () {
                    // scroll to top
                    interface.bodyNode.animate({
                        scrollTop: gc.website.data.roomContainerOffsetTop
                    }, 600);

                });
            },

            mod04roomContainer: function () {

                if (!$().bxSlider) {
                    // plugin not loadeded
                    $.getScript("//cdnjs.cloudflare.com/ajax/libs/bxslider/4.2.5/jquery.bxslider.min.js").done(function (script, textStatus) {
                        mod04rooms();
                    }).fail(function (jqxhr, settings, exception) {
                        console.log("missing bxslider plugin file");
                    });
                } else {
                    // plugin loaded
                    mod04rooms();
                }

                function mod04rooms() {
                    // import general css file
                    $('head').append('<link rel="stylesheet" href="//static.guestcentric.net/cdn/wsbdev/wsbdev_css/mod.roomContainer.04.min.css" type="text/css" />');
                    // $('head').append('<link rel="stylesheet" href="mod.roomContainer.04.css" type="text/css" />');

                    var roomElem = $(".roomContainer"),
                        roomElemParent = roomElem.parent(),
                        roomElemWidth = roomElemParent.width();

                    // create thumbs & removing "active" class
                    roomElemParent.clone(true, true).attr('id', 'roomThumbs').appendTo(roomElemParent.parent()).find(".roomContainer").addClass("roomThumb notClicked " + gc.website.data.roomThumbClass + "").removeAttr("id").removeClass("roomContainer item active");

                    // thumb element
                    var roomThumbContainer = $('.roomThumb');

                    roomThumbContainer.each(function () {
                        // insert minRate into the roomName to display:table-cell
                        $(this).find(".minRate").appendTo($(this).find(".roomName"));
                        // removing don't needed elements 
                        $(this).find(".roomDescription, .floatfixer, .promotions-termsActions, .roomPhoto > a").remove();
                        // removing rel attr to not be opened on the shadowbox script
                        $(this).find(".pgImage > a").removeAttr("rel");
                    });

                    // change element type plugin
                    $.fn.changeElementType = function (newType) {
                        var attrs = {};
                        $.each(this[0].attributes, function (idx, attr) {
                            attrs[attr.nodeName] = attr.nodeValue;
                        });
                        this.replaceWith(function () {
                            return $("<" + newType + "/>", attrs).append($(this).contents());
                        });
                    }
                    // change element type run, because plugin subnav have to be an <a>
                    roomThumbContainer.changeElementType('a');

                    interface.browserWidth = $(window).width();
                    interface.browserHeight = $(window).height();

                    // set roomContainer height
                    if (interface.browserWidth > 768) {

                        $(".roomContainer").css("height", interface.browserHeight - gc.website.data.roomContainerOffsetTop + "px");
                    }

                    // redefine variable
                    roomThumbContainer = $('a.roomThumb');

                    // create sliderNav custom
                    for (var i = 0; i < roomThumbContainer.length; i++) {
                        var roomThumbName = $(roomThumbContainer[i]);
                        // add attribute to slide
                        roomThumbName.attr("data-slide-index", i);
                    }

                    $("#roomtypes").find(roomElem).width(roomElemWidth);

                    // bxSlider run plugin
                    roomElemParent.bxSlider({
                        minSlides: 1,
                        maxSlides: 1,
                        moveSlides: 1,
                        swipeThreshold: 250,
                        pagerCustom: '#roomThumbs',
                        nextText: 'Next room',
                        prevText: 'Previous room'
                    });

                    // add extra class for styling
                    $(".bx-controls").addClass("roomSlideNav");

                    if (!isTouchDevice()) {
                        // hide bx-wrapper
                        roomElem.closest(".bx-wrapper").css("display", "none");

                        roomThumbContainer.bind(interface.clickHandler, function () {
                            // remove initial elem class
                            roomThumbContainer.removeClass("notClicked");
                            // show bx-wrapper
                            roomElem.closest(".bx-wrapper").fadeIn();
                            // click and scroll elem Top
                            interface.bodyNode.animate({
                                scrollTop: roomElem.offset().top - gc.website.data.roomContainerOffsetTop
                            }, 600);
                        });
                    }
                }

            },

            mod05roomContainer: function (_className, newApiKey) {
                // room toggle - revision

                if (!_className) {
                    _className = "";
                }

                if (!newApiKey && !$(interface.bodyNode).hasClass("mobile")) {
                    newApiKey = bginit.vars.apikey;
                }

                $(".roomContainer").each(function () {
                    $(this).find(".roomPhoto").insertBefore($(this).find(".roomName"));
                })

                $(".roomContainer").click(function () {
                    var _this = $(this);

                    $(".roomContainer").addClass("tumb");

                    if (!_this.hasClass("open")) {
                        var _clone = _this.clone(true),
                            bookingbt = _clone.find(".bgLink");

                        // replicate booking button
                        bookingbt.click(function () {
                            var rel = $(this).attr('rel'),
                                paramString = rel.match(/^bglink\[(.*?)\]/i)[1],
                                params = paramString.split(",");

                            window.location.href = bginit.vars.purl + '?apikey=' + newApiKey + '&startDay=' + params[0] + '&nrNights=' + params[1] + '&preselect=' + params[2] + setLinkerParam();
                        });

                        // close all rooms and remove inactive elem
                        $(".roomContainer.room-opened").remove();
                        $(".roomContainer").removeAttr("data-room");
                        // clone, open and position roomContainer elem
                        _clone.prependTo(_this.parent()).addClass("room-opened clone open").removeClass("tumb");
                        // add class to body
                        _this.attr("data-room", "active");

                        gc.website.initGallery(_clone.find(".roomPhoto"));

                        // scroll to top
                        $('html,body').animate({
                            scrollTop: _clone.offset().top - gc.website.data.roomContainerOffsetTop
                        }, 600);

                        $(".roomContainer.tumb").removeAttr("class").addClass("roomContainer item tumb " + _className);
                    }

                });
            },

            simpleABtesting: function (obj) {

                $.getScript("//static.guestcentric.net/cdn/wsbdev/wsbdev_bin/js/plugins/testing/simpleab.min.js").done(function (script, textStatus) {
                    // $.getScript( "../simpleab.js" ).done(function( script, textStatus ) {

                    if (obj == undefined) {

                        obj = {};
                        obj.targetElem = ".gc_bebutton, .shortHandCheckRates, #checkAvailability, .booknow, .bookingGadget";
                        obj.targetElemLabel = "Book now";
                    }

                    // console.log(obj.targetElem);
                    // console.log(obj.targetElemLabel);

                    $.simpleAB({
                        classCount: 2,
                        target: obj.targetElem,
                        label: obj.targetElemLabel,
                        tracker: 'gcTracker',
                    });

                    // sending events to analytics on simpleab.js file

                }).fail(function (jqxhr, settings, exception) {
                    console.log("missing simpleABtesting plugin file");
                });

            },

            moreMenuItems: function () {

                // import general css file
                $('head').append('<link rel="stylesheet" href="//static.guestcentric.net/cdn/wsbdev/wsbdev_css/mod.moreMenuItems.min.css" type="text/css" />');

                // Menu
                var navigationNodeUL = interface.navigationNode.find("ul"),
                    navigationNodeHeight = interface.navigationNode.height();

                if (navigationNodeUL.height() > navigationNodeHeight) {

                    interface.navigationNode.find("li").each(function () {
                        if ($(this).position().top >= navigationNodeHeight) {
                            $(this).addClass("more");
                        }
                    });

                    if (interface.bodyNode.hasClass("lang-pt") || interface.bodyNode.hasClass("lang-br")) {
                        moreLabel = "Mais";
                    } else if (interface.bodyNode.hasClass("lang-es")) {
                        moreLabel = "Más";
                    } else if (interface.bodyNode.hasClass("lang-ca")) {
                        moreLabel = "Més";
                    } else if (interface.bodyNode.hasClass("lang-fr")) {
                        moreLabel = "Plus";
                    } else if (interface.bodyNode.hasClass("lang-it")) {
                        moreLabel = "Più";
                    } else if (interface.bodyNode.hasClass("lang-de")) {
                        moreLabel = "Mehr";
                    } else if (interface.bodyNode.hasClass("lang-el")) {
                        moreLabel = "Περισσότερο";
                    } else if (interface.bodyNode.hasClass("lang-ru")) {
                        moreLabel = "Больше";
                    } else if (interface.bodyNode.hasClass("lang-zh-CN")) {
                        moreLabel = "更多";
                    } else if (interface.bodyNode.hasClass("lang-pl")) {
                        moreLabel = "Jeszcze";
                    } else if (interface.bodyNode.hasClass("lang-tr")) {
                        moreLabel = "Daha";
                    } else if (interface.bodyNode.hasClass("lang-fi")) {
                        moreLabel = "Lisää";
                    } else if (interface.bodyNode.hasClass("lang-lv")) {
                        moreLabel = "Vairāk";
                    } else if (interface.bodyNode.hasClass("lang-sv")) {
                        moreLabel = "Mer";
                    } else if (interface.bodyNode.hasClass("lang-ro")) {
                        moreLabel = "Mai Mult";
                    } else if (interface.bodyNode.hasClass("lang-sl")) {
                        moreLabel = "Viac";
                    } else {
                        moreLabel = "More";
                    }

                    $("<li class='navMoreTrigger'><a class='navMoreItem'>" + moreLabel + "</a></div>").appendTo(navigationNodeUL);
                    $("li.more").wrapAll("<ul class='navMore' />");
                    $(".navMore").prev().addClass("more").prependTo(".navMore");
                    $(".navMore").appendTo(".navMoreTrigger");

                    $(".navMoreTrigger").click(function () {
                        $(".navMore").css("max-height", interface.browserHeight - interface.topBlock.height() + "px");
                        interface.navigationNode.css("overflow", "visible");
                        $(this).toggleClass("opened");
                    });
                }

            },

            isOnScreen: function (obj) {

                // console.log(obj)
                // console.log(obj.targetElem)
                // console.log(obj.targetClass)

                // check if obj variable is set on template js, if not use defaults
                if (obj.targetElem == undefined) {
                    obj.targetElem = ".imagelinkgadget, .roomContainer";
                }
                if (obj.targetClass == undefined) {
                    obj.targetClass = "enabled";
                }

                // console.log(obj.targetElem)
                // console.log(obj.targetClass)

                var windowNode = $(window);

                // to determine if element is on screen
                $.fn.isOnScreen = function () {
                    var viewport = {};
                    viewport.top = windowNode.scrollTop();
                    viewport.bottom = viewport.top + interface.browserHeight;
                    var bounds = {};
                    bounds.top = this.offset().top;
                    bounds.bottom = bounds.top + this.outerHeight();
                    return ((bounds.top <= viewport.bottom) && (bounds.bottom >= viewport.top));
                };

                // adding class to elems on screen
                function showElements(selector) {
                    var counter = 1;

                    $(selector).each(function () {

                        var targetElem = $(this);

                        if (!!targetElem.isOnScreen() && !targetElem.hasClass(obj.targetClass)) {
                            targetElem.addClass(obj.targetClass);
                        }
                    })
                }
                showElements("" + obj.targetElem + "");

                // adding class to elems on screen during the scroll
                windowNode.scroll(function () {
                    showElements("" + obj.targetElem + "");

                });

            },

            //RSS FEED PLUGIN
            blogfeed: function (obj) {

                $.getScript("//static.guestcentric.net/cdn/wsbdev/wsbdev_bin/js/plugins/blog/FeedEk.min.js").done(function (script, textStatus) {

                    $.getScript("//cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js").done(function (script, textStatus) {
                        
                        if (obj.targetElem == undefined) {
                            obj.targetElem = ".content";
                        }
                        
                        // Create element to render rss feed
                        $('<div id="blogfeed" />').appendTo(obj.targetElem)

                        // Rss Feed
                        $('#blogfeed').FeedEk({
                            FeedUrl: obj.FeedUrl,
                            ShowPubDate: obj.ShowPubDate
                        });

                    }).fail(function (jqxhr, settings, exception) {
                        console.log("missing moment.min plugin file");
                    });

                }).fail(function (jqxhr, settings, exception) {
                    console.log("missing FeedEk.min plugin file");
                });
            }




        };

    }());
}());