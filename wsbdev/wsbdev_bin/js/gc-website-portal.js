/*
 
 Websites Javascript Library (portal)
 ****************************
 JQuery 1.10.2
 Author: tiago.o and pedro.d
 Date: 22/12/2015
 Latest update: 07/12/2015 by pedro.d

 */


var gcportal = window.gcportal || {};

(function () {

	gcportal.website = (function () {

		var data = {
				geo: null,
				sliderLoaded : false,
				pages : {
					current : 'Home',
					matchObj : {
						'homepage'                  : 'Homepage',
						'hotel-products'            : 'Products',
						'hotel-gallery'             : 'Gallery',
						'hotel-contacts'            : 'Contacts'
					}
				}
			},

			interface = {
				browserWidth : null,
				browserHeight : null,
				pageWidth : null,
				pageHeight : null
			};

		function setHtmlVars() {

			var bodyNode = $(document.body);
			var htmlObj = {
				bodyNode                : bodyNode,
				topBlock                : $('#topblock'),
				centerBlock             : $('#centerblock'),
				contentBlock            : $('#contentblock'),
				bottomBlock             : $('#bottomblock'),
				bannerNode              : $(".flashContainer"),

				imageLink               : $(".imagelinkgadget"),
				imageLink_Large         : $(".large"),
				imageLink_MediumLarge   : $(".mediumlarge"),
				imageLink_Medium        : $(".medium"),
				imageLink_Small         : $(".small"),
				imageLink_Popup         : $(".popup"),

				hasBanner               : bodyNode.hasClass("with-bannerGadget"),
				hasBannerImg            : bodyNode.hasClass("with-bannerImg"),
				hasNoBanner             : bodyNode.hasClass("no-banner")

			};

			$.extend(interface, htmlObj);
		};

		function setCurrentPage(){
			for (var page in data.pages.matchObj){
				if (interface.bodyNode.hasClass(page)){
					data.pages.current = data.pages.matchObj[page];
					break;
				}
			}
		};

		function resize(){
			interface.browserWidth   = $(window).width();
			interface.browserHeight  = $(window).height();
			interface.pageWidth      = $(document.body).width();
			interface.pageHeight     = $(document.body).height();
		};

		function bannerSize(obj) {
			if (interface.hasBanner || interface.hasBannerPromo || interface.hasBannerImg || data.pages.current == 'Location') {
				interface.centerBlock.css('height', interface.browserHeight - obj.varBannerHeight1 - obj.varBannerHeight2 + "px");
				interface.contentBlock.css("visibility", "visible");
			} else {
				interface.centerBlock.css("height", 0 + "px");
			}
		};

		function mobileClass() {
			if (interface.browserWidth <= 992) {
				$(interface.bodyNode).addClass("mobile");
			} else {
				$(interface.bodyNode).removeClass("mobile");
			}
		};

		function windowScroll (obj){
			//window scroll
			var scroll = $(window).scrollTop();

			if (interface.hasBanner || interface.hasBannerImg) {
				if (scroll > 0) {
					interface.bodyNode.addClass("scrolling");
				} else {
					interface.bodyNode.removeClass("scrolling");
				}
				if (scroll > obj.varScroll1) {
					interface.bodyNode.addClass("scroll1");
				} else {
					interface.bodyNode.removeClass("scroll1");
				}
				if (scroll > obj.varScroll2) {
					interface.bodyNode.addClass("scroll2");
				} else {
					interface.bodyNode.removeClass("scroll2");
				}
				if (scroll > interface.browserHeight) {
					interface.bodyNode.addClass("stage1");
				} else {
					interface.bodyNode.removeClass("stage1");
				}
			} 

		};

		function handlerEvents(obj){

			var _object = obj;

			//event resize
			$( window ).resize(function() {
				resize();
				bannerSize(_object);
				mobileClass();
			});

			//event scroll
			$( window ).scroll(function() {
				windowScroll(_object);
			});

			//button scroll
			$('#scroll').click(function() {
				if (!interface.bodyNode.hasClass('scrolling')) {
					$('html,body').animate({
						scrollTop: interface.browserHeight - _object.scrollFixedOffset
					}, 600);
				} else {
					$('html,body').animate({
						scrollTop: 0
					}, 600);
				}
			});

			// add class hover on link
			interface.imageLink.hover(function() {
				if (!$(this).hasClass("nolink") == true) {
					$(this).addClass("hover");
				}
			}, function() {
				$(this).removeClass("hover");
			});

		};

		function handlerDetails(obj){

			var _object = obj;

			// banner size
			bannerSize(obj);

			// add class if mobile
			mobileClass(obj);

			// bigger photo
			$(".pgImage").each(function() {
				$(this).css('background-image', 'url("' + $(this).find("a").attr('href') + '")');
			});

			$(".pgText").click( function() {
				$(this).siblings(".pgImage").find("> a:first-child")[0].click();
			});

			// imageLink popup
			if (interface.imageLink_Popup.length > 0) {
				var overlay = $("<div class='overlay'></div>");

				interface.bodyNode.addClass("popupWindow");
				interface.bodyNode.append(interface.imageLink_Popup);
				interface.bodyNode.append(overlay);
				interface.bodyNode.click(function() {
					interface.bodyNode.removeClass("popupWindow");
					interface.imageLink_Popup.remove();
					overlay.remove();
				});
			}

			// add id attr to elems
			interface.imageLink_Small.each(function(i) {
				$(this).addClass('small' + i);
			});
			interface.imageLink_Medium.each(function(i) {
				$(this).addClass('medium' + i);
			});
			interface.imageLink_MediumLarge.each(function(i) {
				$(this).addClass('mediumlarge' + i);
			});
			interface.imageLink_Large.each(function(i) {
				$(this).addClass('large' + i);
			});

			interface.imageLink.find("a").each(function() {
				// creating nolink type
				if ($(this).find(".imgLink span:contains('nolink')").length > 0) {
					$(this).removeAttr("href");
					$(this).closest(".imagelinkgadget").addClass("nolink");
				}
				// creating noimage type
				if ($(this).find(".imgLink span:contains('noimage')").length > 0) {
					$(this).closest(".imagelinkgadget").addClass("noimage");
				}
				// creating notext type
				if ($(this).find(".imgLink span:contains('notext')").length > 0) {
					$(this).closest(".imagelinkgadget").addClass("notext");
					$(this).removeAttr("href");
				}
				
			});
		};


		function loading() {
		
			if (interface.hasNoBanner || interface.hasBannerImg){
				$("#loader").fadeOut("slow");
				return;
			}

			var checker;
			checker = setInterval(function() {
				if (!interface.hasBannerImg && $(".gcb_container_sized").length > 0) {
					var displayLoad = $('.gcb_image_loading').css('display');		
					if(displayLoad == 'none'){
						$("#loader").fadeOut("slow");
						interface.bodyNode.addClass("loaded");
						clearInterval(checker);
					}
				}
			}, 250);
		};

		// public functions
		return {

			data : null,

			init : function (obj, callback) {

				this.data = obj;

				setHtmlVars();
				resize();
				setCurrentPage();
				handlerDetails(obj);
				handlerEvents(obj);
				loading();

				if (callback)
					callback();

			},

			getHtmlNodes : function () {
				return interface;
			},

			getData : function () {
				return data;
			},

			galleryUnite : function(lib, obj) {

				$.getScript( "http://static.guestcentric.net/cdn/wsbdev/wsbdev_bin/js/plugins/gallery/unitegallery.min.js" ).done(function( script, textStatus ) {
					// import general css file
					$('head').append('<link rel="stylesheet" href="http://static.guestcentric.net/cdn/wsbdev/wsbdev_bin/js/plugins/gallery/unite-gallery.css" type="text/css" />');

					$.getScript( "http://static.guestcentric.net/cdn/wsbdev/wsbdev_bin/js/plugins/gallery/"+lib+"/ug-theme-"+lib+".js" ).done(function( script, textStatus ) {
						// redo html structure
						$(".pgImage > a").each( function() {
							var imageURL = $(this).attr("href"),
								imageTitle = $(this).attr("title");

							$("#pg").append('<img alt="'+imageTitle+'" src="'+imageURL+'" data-image="'+imageURL+'" data-description="'+imageTitle+'" />');
						});
						// remove old gallery elem
						$(".pgContainer").remove();
						// run plugin
						$("#pg").unitegallery(obj);

					}).fail(function( jqxhr, settings, exception ) {
						console.log("missing galleryUnite plugin file");
					});

				}).fail(function( jqxhr, settings, exception ) {
					console.log("missing galleryUnite plugin file");
				});
				
			},

			imglinkImageBg : function() {
				$(".imgImageWrapper").each(function() {
					$(this).css('background-image', 'url("' + $(this).find("img").attr('src') + '")');
					$(this).find("img").css("display", "none");
				});
			},

		};

	}());
}());